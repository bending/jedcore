package com.jedk1.jedcore.ability.gift;

import com.jedk1.jedcore.configuration.JedCoreConfig;
import com.jedk1.jedcore.util.TempFallingBlock;
import com.projectkorra.projectkorra.Element;
import com.projectkorra.projectkorra.GeneralMethods;
import com.projectkorra.projectkorra.ability.AddonAbility;
import com.projectkorra.projectkorra.command.Commands;
import com.projectkorra.projectkorra.util.DamageHandler;
import com.projectkorra.projectkorra.util.TempArmor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CandyOrDeath extends GiftAbility implements AddonAbility {

    private long cooldown;
    private long duration;
    private int killChance;
    private TempFallingBlock pump;

    public CandyOrDeath(Player player) {
        super(player);
        if (bPlayer.isOnCooldown("CandyOrDeath")) {
            return;
        }
        setFields();
        if (GeneralMethods.isRegionProtectedFromBuild(this, player.getTargetBlock(getTransparentMaterialSet(), 40).getLocation())) {
            return;
        }
        bPlayer.addCooldown("CandyOrDeath", getCooldown());
        Location location = player.getEyeLocation().clone().add(player.getEyeLocation().getDirection().multiply(1));
        float yaw = player.getLocation().getYaw();
        int look = yaw > 45 && yaw <= 135 ? 1 : (yaw > 135 && yaw <= 180) || (yaw > -180 && yaw <= -135) ? 2 : (yaw > -135 && yaw <= -90) || (yaw > -90 && yaw <= -45) ? 3 : 0;
        pump = new TempFallingBlock(location, Material.PUMPKIN, (byte) look, location.getDirection().multiply(3), this);
        pump.getFallingBlock().setGlowing(true);
        start();
    }

    @Override
    public void progress() {
        if (player == null || !player.isOnline()) {
            pump.remove();
            remove();
            return;
        }
        if (pump.getFallingBlock().isDead()) {
            remove();
            return;
        }
        if (GeneralMethods.isRegionProtectedFromBuild(this, pump.getLocation())) {
            remove();
            return;
        }

        for (Entity entity : GeneralMethods.getEntitiesAroundPoint(pump.getLocation(), 2.5)) {
            if (entity instanceof LivingEntity && !(entity instanceof ArmorStand) && entity.getEntityId() != player.getEntityId() && !GeneralMethods.isRegionProtectedFromBuild(this, entity.getLocation()) && !((entity instanceof Player) && Commands.invincible.contains(((Player) entity).getName()))) {
                if (Math.random() * 100 < killChance)
                    DamageHandler.damageEntity(entity, ((LivingEntity) entity).getHealth() * 100, this);
                else if (entity instanceof Player)
                    new TempArmor((Player) entity, duration, this, new ItemStack[]{null, null, null, new ItemStack(Material.PUMPKIN)});
                else
                    ((LivingEntity) entity).getEquipment().setArmorContents(new ItemStack[]{null, null, null, new ItemStack(Material.PUMPKIN)});
            }
        }
    }

    @Override
    public boolean isSneakAbility() {
        return false;
    }

    @Override
    public boolean isHarmlessAbility() {
        return false;
    }

    @Override
    public boolean isIgniteAbility() {
        return false;
    }

    @Override
    public boolean isExplosiveAbility() {
        return false;
    }

    @Override
    public long getCooldown() {
        return cooldown;
    }

    @Override
    public String getName() {
        return "CandyOrDeath";
    }

    @Override
    public Location getLocation() {
        return pump.getLocation();
    }

    @Override
    public void load() {
    }

    @Override
    public void stop() {
    }

    @Override
    public String getAuthor() {
        return "CKATEPTb";
    }

    @Override
    public String getVersion() {
        return "1.0";
    }

    @Override
    public boolean isEnabled() {
        ConfigurationSection config = JedCoreConfig.getConfig(this.player);
        return config.getBoolean("Abilities.Gift.CandyOrDeath.Enabled");
    }

    public void setFields() {
        ConfigurationSection config = JedCoreConfig.getConfig(this.player);
        duration = config.getLong("Abilities.Gift.CandyOrDeath.Duration");
        killChance = config.getInt("Abilities.Gift.CandyOrDeath.Chance");
        cooldown = config.getLong("Abilities.Gift.CandyOrDeath.Cooldown");
    }


    @Override
    public String getDescription() {
        return "Вы выпускаете дух хелоуина в виде тыквы, которая нарядит или убьет цель.";
    }
}
